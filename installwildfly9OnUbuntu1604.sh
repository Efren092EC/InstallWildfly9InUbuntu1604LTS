#!/bin/bash

# If add-apt-repository is not installed currently, you can uncomment de line below
#sudo apt-get install software-properties-common python-software-properties

# General utils
sudo apt-get -y update
sudo apt-get -y upgrade
sudo apt-get -y install unzip
sudo apt-get -y install software-properties-common python-software-properties

#Add WebUpd8 PPA:
sudo add-apt-repository ppa:webupd8team/java
# Update local package index to latest changes in repositories:
sudo apt-get update

# Install JDK 8:
sudo apt-get install oracle-java8-installer
java -version

#Set Oracle JDK 8 as default Java:
sudo apt-get install oracle-java8-set-default

#Download WildFly:
# You can change de URL in order to dowload a other version 
wget http://download.jboss.org/wildfly/9.0.2.Final/wildfly-9.0.2.Final.zip

# Unpack WildFly:
sudo unzip wildfly-9.0.2.Final.zip -d /opt/

# Create symbolic link in order to simplify WildFly updates in future:
sudo ln -s /opt/wildfly-9.0.2.Final /opt/wildfly

# Copy and edit init.d script configuration file:

sudo cp /opt/wildfly/bin/init.d/wildfly.conf /etc/default/wildfly

# Changes to configure:
sudo sed -i 's|# JAVA_HOME="/usr/lib/jvm/default-java"|JAVA_HOME="/usr/lib/jvm/java-8-oracle"|g' /etc/default/wildfly
sudo sed -i 's|# JBOSS_HOME="/opt/wildfly"|JBOSS_HOME="/opt/wildfly-9.0.2.Final"|g' /etc/default/wildfly
sudo sed -i 's|# JBOSS_USER=wildfly|JBOSS_USER=wildfly|g' /etc/default/wildfly
sudo sed -i 's|# JBOSS_MODE=standalone|JBOSS_MODE=standalone|g' /etc/default/wildfly
sudo sed -i 's|# JBOSS_CONFIG=standalone.xml|JBOSS_CONFIG=standalone.xml|g' /etc/default/wildfly
sudo sed -i 's|# STARTUP_WAIT=60|STARTUP_WAIT=60|g' /etc/default/wildfly
sudo sed -i 's|# SHUTDOWN_WAIT=60|SHUTDOWN_WAIT=60|g' /etc/default/wildfly
sudo sed -i 's|# JBOSS_CONSOLE_LOG="/var/log/wildfly/console.log"|JBOSS_CONSOLE_LOG="/var/log/wildfly/console.log"|g' /etc/default/wildfly

# Wildfly Init file
sudo cp /opt/wildfly/bin/init.d/wildfly-init-debian.sh /etc/init.d/wildfly
sudo chown root:root /etc/init.d/wildfly
sudo chmod +X /etc/init.d/wildfly

# Changes in Init
sudo update-rc.d wildfly defaults
sudo update-rc.d wildfly enable

# Add system user to run WildFly
sudo useradd --system --shell /bin/false wildfly

sudo mkdir -p /var/log/wildfly
sudo chown -R wildfly:wildfly /opt/wildfly-9.0.2.Final/
sudo chown -R wildfly:wildfly /opt/wildfly
sudo chown -R wildfly:wildfly /var/log/wildfl

# Tunning
sudo sed -i 's|JAVA_OPTS="-Xms64m -Xmx512m -XX:MaxPermSize=256m -Djava.net.preferIPv4Stack=true"|JAVA_OPTS="-Xms1024m -Xmx1024m -XX:MaxPermSize=256m -XX:NewRatio=2 -XX:PermSize=64m -Djava.net.preferIPv4Stack=true"'

sudo service wildfly start
sudo service wildfly status